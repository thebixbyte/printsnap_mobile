(function e(t, n, r) {
  function s(o, u) {
    if (!n[o]) {
      if (!t[o]) {
        var a = typeof require == 'function' && require;
        if (!u && a)
          return a(o, !0);
        if (i)
          return i(o, !0);
        throw new Error('Cannot find module \'' + o + '\'');
      }
      var f = n[o] = { exports: {} };
      t[o][0].call(f.exports, function (e) {
        var n = t[o][1][e];
        return s(n ? n : e);
      }, f, f.exports, e, t, n, r);
    }
    return n[o].exports;
  }
  var i = typeof require == 'function' && require;
  for (var o = 0; o < r.length; o++)
    s(r[o]);
  return s;
}({
  1: [
    function (require, module, exports) {
      //bix_hlink = "http://127.0.0.1:1357";
      bix_hlink = 'http://41.89.162.4:3000';
      app = require('./js/app.js');
      /*
app.config(['$stateProvider','$urlRouterProvider', '$compileProvider',  
	function($stateProvider, $urlRouterProvider, $compileProvider) {

   $stateProvider
    .state('Home', {
            url: '/',
            controller: 'uploadViewController.ctrl.js', 
            templateUrl: 'views/upload-view.html'
   });
    
	$urlRouterProvider.otherwise("/");

	$compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|content|file|assets-library):/);
  
        
}])
*/
      app.run(function ($rootScope, $ionicPlatform) {
        $rootScope.appReady = { status: false };
        $ionicPlatform.ready(function () {
          console.log('ionic Ready');
          $rootScope.appReady.status = true;
          $rootScope.$apply();
          console.log('in app.js, appReady is ' + $rootScope.appReady.status);
          if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
          }
          if (window.StatusBar) {
            StatusBar.styleDefault();  //StatusBar.hide();
          }
        });
      });
      require('./js/directives/directives.js');
      require('./js/controllers/controllers.js');
      require('./js/services/services.js');
    },
    {
      './js/app.js': 2,
      './js/controllers/controllers.js': 4,
      './js/directives/directives.js': 9,
      './js/services/services.js': 13
    }
  ],
  2: [
    function (require, module, exports) {
      // Ionic Starter App
      // angular.module is a global place for creating, registering and retrieving Angular modules
      // 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
      // the 2nd parameter is an array of 'requires'
      module.exports = angular.module('picSnap', [
        'ionic',
        'ngCordova'
      ]);
    },
    {}
  ],
  3: [
    function (require, module, exports) {
      app.controller('appHeaderController', [
        '$scope',
        '$http',
        function ($scope, $http) {
          var printUser = localStorage.getItem('printUser');
          var printEmail = localStorage.getItem('printEmail');
          $scope.data = {};
          $scope.data.printUser = printUser == undefined ? null : printUser;
          $scope.data.printEmail = printEmail == undefined ? null : printEmail;
        }
      ]);
    },
    {}
  ],
  4: [
    function (require, module, exports) {
      (function e(t, n, r) {
        function s(o, u) {
          if (!n[o]) {
            if (!t[o]) {
              var a = typeof require == 'function' && require;
              if (!u && a)
                return a(o, !0);
              if (i)
                return i(o, !0);
              throw new Error('Cannot find module \'' + o + '\'');
            }
            var f = n[o] = { exports: {} };
            t[o][0].call(f.exports, function (e) {
              var n = t[o][1][e];
              return s(n ? n : e);
            }, f, f.exports, e, t, n, r);
          }
          return n[o].exports;
        }
        var i = typeof require == 'function' && require;
        for (var o = 0; o < r.length; o++)
          s(r[o]);
        return s;
      }({
        1: [
          function (require, module, exports) {
            app.controller('appHeaderController', [
              '$scope',
              '$http',
              function ($scope, $http) {
                var printUser = localStorage.getItem('printUser');
                var printEmail = localStorage.getItem('printEmail');
                $scope.data = {};
                $scope.data.printUser = printUser == undefined ? null : printUser;
                $scope.data.printEmail = printEmail == undefined ? null : printEmail;
              }
            ]);
          },
          {}
        ],
        2: [
          function (require, module, exports) {
            require('./app-header.ctrl.js');
            require('./signup-pages.ctrl.js');
            require('./pic-snap.ctrl.js');
            require('./upload-view.ctrl.js');
          },
          {
            './app-header.ctrl.js': 1,
            './pic-snap.ctrl.js': 3,
            './signup-pages.ctrl.js': 4,
            './upload-view.ctrl.js': 5
          }
        ],
        3: [
          function (require, module, exports) {
            app.controller('picSnap', [
              '$scope',
              '$http',
              '$location',
              '$ionicPopup',
              function ($scope, $http, $location, $ionicPopup) {
                //*Checks if the user is logedin
                $scope.signup = localStorage.getItem('printUser') == undefined ? true : false;
                //*Provides a custom show alert function
                $scope.showAlert = function (title, template, cb, ok) {
                  var alertPopup = $ionicPopup.alert({
                      title: title,
                      template: template,
                      okText: ok || 'OK'
                    });
                  alertPopup.then(function (res) {
                    cb(res);
                  });
                };
              }
            ]);
          },
          {}
        ],
        4: [
          function (require, module, exports) {
            app.controller('signupPagesController', [
              '$scope',
              '$http',
              '$location',
              function ($scope, $http, $location) {
                //!Setup the logedin variable for the control of signup page display
                $scope.logedin = localStorage.getItem('printUser') == undefined ? false : true;
                if ($scope.logedin) {
                  $scope.data.printUser = localStorage.getItem('printUser');
                  $scope.data.printEmail = localStorage.getItem('printEmail');
                }
                var Aresponse = function (r) {
                };
                //!Setup the scope variables
                var maxPages = 4;
                var current = 0;
                var pages = [
                    '#s_login',
                    '#s_register',
                    '#s_fin'
                  ];
                //*Form bound variables
                $scope.sp = {};
                //*Application data holder
                $scope.data = $scope.data || {};
                //!Handle requests for the display of the next slide
                $scope.next = function () {
                  if (current + 1 < maxPages) {
                    current++;
                    $(pages[current]).focus();
                  } else if (current + 1 == maxPages) {
                    current = 0;
                    //alert("You should be routed to the startup page");
                    location.reload();
                  } else {
                    current = 0;
                    $(pages[current]).focus();
                  }
                };
                //!Handle requests for the display of the previous slide
                $scope.prev = function () {
                  alert();
                  if (current - 1 > 0) {
                    current--;
                    $(pages[current]).focus();
                  } else if (current - 1 < 0) {
                    current = 0;
                    //alert("You should be routed to the startup page");
                    $(pages[current]).focus();
                  } else {
                    current = 0;
                    $(pages[current]).focus();
                  }
                };
                //!Handle slide change occurances  
                //Expects the current page of the slider sequence [ zero indexed ]
                $scope.slideHasChanged = function (slide_pos) {
                  //alert( "You are now on slide " + slide_pos )
                  current = slide_pos;  //console.dir($location)
                };
                $scope.index = function () {
                  return current;
                };
                //GTEL API RELATORS
                //!PERFORM A LOGIN
                $scope.s_dologin = function () {
                  $.ajax({
                    method: 'POST',
                    url: bix_hlink + '/login',
                    data: {
                      username: $scope.sp.username,
                      password: $scope.sp.password
                    },
                    success: function (response) {
                      //*Handle a successful request
                      console.log('\n\nSuccessfully performed a CORS login request\n\n');
                      //console.dir( response )
                      if (response.status === 200) {
                        //Perform a login
                        $scope.data.printUser = $scope.sp.username;
                        $scope.data.printEmail = $scope.sp.email;
                        localStorage.setItem('printUser', $scope.sp.username);
                        localStorage.setItem('printEmail', response.email);
                        console.log('Login complete!!');
                      } else {
                        //Inform the client of the error
                        $scope.sp.errormessage = response.message;
                        //alert( response.message )
                        $scope.showAlert('Authentication Error', response.message, Aresponse, 'TRY AGAIN');
                      }
                    },
                    error: function (response) {
                      //*Handle an unsuccessful request
                      //alert("\n\nFailed to perform a CORS signout request\n\n")
                      //alert( "Failed to connect to the main server" )
                      $scope.showAlert('Connection error', '<center>Failed to establish a working internet connection</center>', Aresponse, 'TRY AGAIN');
                    }
                  });
                };
                //!PERFORM A SIGNUP
                $scope.s_dosignup = function () {
                  $.ajax({
                    method: 'POST',
                    url: bix_hlink + '/signup',
                    data: {
                      username: $scope.sp.username,
                      password: $scope.sp.password,
                      password2: $scope.sp.password2,
                      email: $scope.sp.email
                    },
                    success: function (response) {
                      //*Handle a successful request
                      console.log('\n\nSuccessfully performed a CORS signup request\n\n');
                      if (response.status === 200) {
                        //Perform a login
                        $scope.data.printUser = $scope.sp.username;
                        $scope.data.printEmail = $scope.sp.email;
                        localStorage.setItem('printUser', $scope.sp.username);
                        localStorage.setItem('printEmail', $scope.sp.email);
                      } else {
                        //Inform of the error
                        $scope.sp.errormessage = response.message;
                        //alert( response.message )
                        $scope.showAlert('Authentication Error', response.message, Aresponse, 'TRY AGAIN');
                      }
                    },
                    error: function (response) {
                      //*Handle an unsuccessful request
                      //alert("\n\nFailed to perform a CORS signup request\n\n")
                      $scope.showAlert('Connection error', '<center>Failed to establish a working internet connection</center>', Aresponse, 'TRY AGAIN');
                    }
                  });
                };
              }
            ]);
          },
          {}
        ],
        5: [
          function (require, module, exports) {
            app.controller('uploadViewController', [
              '$scope',
              '$rootScope',
              '$cordovaCamera',
              function ($scope, $rootScope, $cordovaCamera) {
                $scope.sp.ready = false;
                $scope.sp = $scope.sp || {};
                $scope.images = [];
                $scope.myaddr = bix_hlink;
                $scope.sp.username = typeof localStorage.getItem('printUser') == 'undefined' ? '' : localStorage.getItem('printUser');
                $scope.sp.album = typeof localStorage.getItem('printAlbum') == 'undefined' ? 'myAlbum' : localStorage.getItem('printAlbum');
                $scope.color = 'black';
                $scope.lmessage = '';
                $scope.umessage = '';
                $rootScope.$watch('appReady.status', function () {
                  console.log('watch fired ' + $rootScope.appReady.status);
                  if ($rootScope.appReady.status) {
                    $scope.sp.ready = true;
                  }
                });
                $scope.selImages = function () {
                  window.imagePicker.getPictures(function (results) {
                    for (var i = 0; i < results.length; i++) {
                      console.log('Image URI: ' + results[i]);
                      $scope.images.push(results[i]);
                    }
                    if (!$scope.$$phase) {
                      $scope.$apply();
                    }
                  }, function (error) {
                    console.log('Error: ' + error);
                  });
                };
                $scope.cImgcb = function (btn) {
                  if (btn === 1) {
                    $scope.images = [];
                  }
                };
                $scope.clearImages = function () {
                  //alert("I can get here but do nothing")
                  var r = confirm('Continuing with this will clear all your selected images.\n\nDo you want to continue?');
                  if (r == true) {
                    $scope.cImgcb(1);
                  }  //navigator.notification.confirm( "Continuing with this will clear all your selected images.\n\nDo you want to continue?", cImgcb, "Clear Image Selection?", ["Yes", "No"]);
                     //$scope.images = []; 
                };
                $scope.isMultiple = function () {
                  return $scope.images.length > 0 ? true : false;
                };
                $scope.uploadImages = function () {
                  if ($scope.sp.album === undefined) {
                    $scope.sp.album = 'default_album';
                    alert('setting up default album title');
                  }
                  var ft = new FileTranfer();
                  var options;
                  for (imageURI in $scope.images) {
                    $scope.umessage = 'Uploading ' + $scope.images[imageURI];
                    options = new FileUploadOptions();
                    options.fileKey = 'file', options.fileName = $scope.images[imageURI].substr($scope.images[imageURI].lastIndexOf('/') + 1);
                    options.mimeType = 'image/jpeg';
                    var params = new Object();
                    params.g_user = $scope.g_user;
                    params.g_album = $scope.sp.album;
                    options.params = params;
                    options.chunkedMode = false;
                    ft.upload($scope.myaddr + '/gtel/api/photo', $scope.win, $scope.fail, options);
                    alert('uploading image ' + $scope.images[imageURI].substr($scope.images[imageURI].lastIndexOf('/') + 1));
                  }
                };
                $scope.updateUser = function (user) {
                  //console.log(user);
                  $scope.sp.username = user;
                };
                $scope.updateAlbum = function (album) {
                  //console.log(album);
                  $scope.sp.album = album;
                };
                $scope.doLogon = function () {
                  if ($scope.g_user.length < 2 || $scope.sp.album.length < 2) {
                    $scope.color = 'red';
                    $scope.lmessage = 'Please provide valid credentials';
                  } else {
                    localStorage.setItem('printAlbum', $scope.sp.album);
                    localStorage.setItem('printUser', $scope.sp.username);  //window.location = "#upload" 
                  }
                };
                $scope.win = function (r) {
                  alert('SERVER: ' + r);
                };
                $scope.fail = function (err) {
                  alert('SERVER: ' + err);
                };  //Do something
              }
            ]);
          },
          {}
        ]
      }, {}, [2]));
    },
    {
      './app-header.ctrl.js': 3,
      './pic-snap.ctrl.js': 5,
      './signup-pages.ctrl.js': 6,
      './upload-view.ctrl.js': 7
    }
  ],
  5: [
    function (require, module, exports) {
      app.controller('picSnap', [
        '$scope',
        '$http',
        '$location',
        '$ionicPopup',
        function ($scope, $http, $location, $ionicPopup) {
          //*Checks if the user is logedin
          $scope.signup = localStorage.getItem('printUser') == undefined ? true : false;
          //*Provides a custom show alert function
          $scope.showAlert = function (title, template, cb, ok) {
            var alertPopup = $ionicPopup.alert({
                title: title,
                template: template,
                okText: ok || 'OK'
              });
            alertPopup.then(function (res) {
              cb(res);
            });
          };
        }
      ]);
    },
    {}
  ],
  6: [
    function (require, module, exports) {
      app.controller('signupPagesController', [
        '$scope',
        '$http',
        '$location',
        function ($scope, $http, $location) {
          //!Setup the logedin variable for the control of signup page display
          $scope.logedin = localStorage.getItem('printUser') == undefined ? false : true;
          if ($scope.logedin) {
            $scope.data.printUser = localStorage.getItem('printUser');
            $scope.data.printEmail = localStorage.getItem('printEmail');
          }
          var Aresponse = function (r) {
          };
          //!Setup the scope variables
          var maxPages = 4;
          var current = 0;
          var pages = [
              '#s_login',
              '#s_register',
              '#s_fin'
            ];
          //*Form bound variables
          $scope.sp = {};
          //*Application data holder
          $scope.data = $scope.data || {};
          //!Handle requests for the display of the next slide
          $scope.next = function () {
            if (current + 1 < maxPages) {
              current++;
              $(pages[current]).focus();
            } else if (current + 1 == maxPages) {
              current = 0;
              //alert("You should be routed to the startup page");
              location.reload();
            } else {
              current = 0;
              $(pages[current]).focus();
            }
          };
          //!Handle requests for the display of the previous slide
          $scope.prev = function () {
            alert();
            if (current - 1 > 0) {
              current--;
              $(pages[current]).focus();
            } else if (current - 1 < 0) {
              current = 0;
              //alert("You should be routed to the startup page");
              $(pages[current]).focus();
            } else {
              current = 0;
              $(pages[current]).focus();
            }
          };
          //!Handle slide change occurances  
          //Expects the current page of the slider sequence [ zero indexed ]
          $scope.slideHasChanged = function (slide_pos) {
            //alert( "You are now on slide " + slide_pos )
            current = slide_pos;  //console.dir($location)
          };
          $scope.index = function () {
            return current;
          };
          //GTEL API RELATORS
          //!PERFORM A LOGIN
          $scope.s_dologin = function () {
            $.ajax({
              method: 'POST',
              url: bix_hlink + '/login',
              data: {
                username: $scope.sp.username,
                password: $scope.sp.password
              },
              success: function (response) {
                //*Handle a successful request
                console.log('\n\nSuccessfully performed a CORS login request\n\n');
                //console.dir( response )
                if (response.status === 200) {
                  //Perform a login
                  $scope.data.printUser = $scope.sp.username;
                  $scope.data.printEmail = $scope.sp.email;
                  localStorage.setItem('printUser', $scope.sp.username);
                  localStorage.setItem('printEmail', response.email);
                  console.log('Login complete!!');
                } else {
                  //Inform the client of the error
                  $scope.sp.errormessage = response.message;
                  //alert( response.message )
                  $scope.showAlert('Authentication Error', response.message, Aresponse, 'TRY AGAIN');
                }
              },
              error: function (response) {
                //*Handle an unsuccessful request
                //alert("\n\nFailed to perform a CORS signout request\n\n")
                //alert( "Failed to connect to the main server" )
                $scope.showAlert('Connection error', '<center>Failed to establish a working internet connection</center>', Aresponse, 'TRY AGAIN');
              }
            });
          };
          //!PERFORM A SIGNUP
          $scope.s_dosignup = function () {
            $.ajax({
              method: 'POST',
              url: bix_hlink + '/signup',
              data: {
                username: $scope.sp.username,
                password: $scope.sp.password,
                password2: $scope.sp.password2,
                email: $scope.sp.email
              },
              success: function (response) {
                //*Handle a successful request
                console.log('\n\nSuccessfully performed a CORS signup request\n\n');
                if (response.status === 200) {
                  //Perform a login
                  $scope.data.printUser = $scope.sp.username;
                  $scope.data.printEmail = $scope.sp.email;
                  localStorage.setItem('printUser', $scope.sp.username);
                  localStorage.setItem('printEmail', $scope.sp.email);
                } else {
                  //Inform of the error
                  $scope.sp.errormessage = response.message;
                  //alert( response.message )
                  $scope.showAlert('Authentication Error', response.message, Aresponse, 'TRY AGAIN');
                }
              },
              error: function (response) {
                //*Handle an unsuccessful request
                //alert("\n\nFailed to perform a CORS signup request\n\n")
                $scope.showAlert('Connection error', '<center>Failed to establish a working internet connection</center>', Aresponse, 'TRY AGAIN');
              }
            });
          };
        }
      ]);
    },
    {}
  ],
  7: [
    function (require, module, exports) {
      app.controller('uploadViewController', [
        '$scope',
        '$rootScope',
        '$cordovaCamera',
        function ($scope, $rootScope, $cordovaCamera) {
          $scope.sp.ready = false;
          $scope.sp = $scope.sp || {};
          $scope.images = [];
          $scope.myaddr = bix_hlink;
          $scope.sp.username = typeof localStorage.getItem('printUser') == 'undefined' ? '' : localStorage.getItem('printUser');
          $scope.sp.album = typeof localStorage.getItem('printAlbum') == 'undefined' ? 'myAlbum' : localStorage.getItem('printAlbum');
          $scope.color = 'black';
          $scope.lmessage = '';
          $scope.umessage = '';
          $rootScope.$watch('appReady.status', function () {
            console.log('watch fired ' + $rootScope.appReady.status);
            if ($rootScope.appReady.status) {
              $scope.sp.ready = true;
            }
          });
          $scope.selImages = function () {
            window.imagePicker.getPictures(function (results) {
              for (var i = 0; i < results.length; i++) {
                console.log('Image URI: ' + results[i]);
                $scope.images.push(results[i]);
              }
              if (!$scope.$$phase) {
                $scope.$apply();
              }
            }, function (error) {
              console.log('Error: ' + error);
            });
          };
          $scope.cImgcb = function (btn) {
            if (btn === 1) {
              $scope.images = [];
            }
          };
          $scope.clearImages = function () {
            //alert("I can get here but do nothing")
            var r = confirm('Continuing with this will clear all your selected images.\n\nDo you want to continue?');
            if (r == true) {
              $scope.cImgcb(1);
            }  //navigator.notification.confirm( "Continuing with this will clear all your selected images.\n\nDo you want to continue?", cImgcb, "Clear Image Selection?", ["Yes", "No"]);
               //$scope.images = []; 
          };
          $scope.isMultiple = function () {
            return $scope.images.length > 0 ? true : false;
          };
          $scope.uploadImages = function () {
            if ($scope.sp.album === undefined) {
              $scope.sp.album = 'default_album';
              alert('setting up default album title');
            }
            var ft = new FileTranfer();
            var options;
            for (imageURI in $scope.images) {
              $scope.umessage = 'Uploading ' + $scope.images[imageURI];
              options = new FileUploadOptions();
              options.fileKey = 'file', options.fileName = $scope.images[imageURI].substr($scope.images[imageURI].lastIndexOf('/') + 1);
              options.mimeType = 'image/jpeg';
              var params = new Object();
              params.g_user = $scope.g_user;
              params.g_album = $scope.sp.album;
              options.params = params;
              options.chunkedMode = false;
              ft.upload($scope.myaddr + '/gtel/api/photo', $scope.win, $scope.fail, options);
              alert('uploading image ' + $scope.images[imageURI].substr($scope.images[imageURI].lastIndexOf('/') + 1));
            }
          };
          $scope.updateUser = function (user) {
            //console.log(user);
            $scope.sp.username = user;
          };
          $scope.updateAlbum = function (album) {
            //console.log(album);
            $scope.sp.album = album;
          };
          $scope.doLogon = function () {
            if ($scope.g_user.length < 2 || $scope.sp.album.length < 2) {
              $scope.color = 'red';
              $scope.lmessage = 'Please provide valid credentials';
            } else {
              localStorage.setItem('printAlbum', $scope.sp.album);
              localStorage.setItem('printUser', $scope.sp.username);  //window.location = "#upload" 
            }
          };
          $scope.win = function (r) {
            alert('SERVER: ' + r);
          };
          $scope.fail = function (err) {
            alert('SERVER: ' + err);
          };  //Do something
        }
      ]);
    },
    {}
  ],
  8: [
    function (require, module, exports) {
      app.directive('appHeader', function () {
        return {
          restrict: 'E',
          controller: 'appHeaderController',
          templateUrl: 'views/app-header.html'
        };
      });
    },
    {}
  ],
  9: [
    function (require, module, exports) {
      (function e(t, n, r) {
        function s(o, u) {
          if (!n[o]) {
            if (!t[o]) {
              var a = typeof require == 'function' && require;
              if (!u && a)
                return a(o, !0);
              if (i)
                return i(o, !0);
              throw new Error('Cannot find module \'' + o + '\'');
            }
            var f = n[o] = { exports: {} };
            t[o][0].call(f.exports, function (e) {
              var n = t[o][1][e];
              return s(n ? n : e);
            }, f, f.exports, e, t, n, r);
          }
          return n[o].exports;
        }
        var i = typeof require == 'function' && require;
        for (var o = 0; o < r.length; o++)
          s(r[o]);
        return s;
      }({
        1: [
          function (require, module, exports) {
            app.directive('appHeader', function () {
              return {
                restrict: 'E',
                controller: 'appHeaderController',
                templateUrl: 'views/app-header.html'
              };
            });
          },
          {}
        ],
        2: [
          function (require, module, exports) {
            require('./app-header.dir.js');
            require('./signup-pages.dir.js');
            require('./spin.dir.js');
            require('./upload-view.dir.js');
          },
          {
            './app-header.dir.js': 1,
            './signup-pages.dir.js': 3,
            './spin.dir.js': 4,
            './upload-view.dir.js': 5
          }
        ],
        3: [
          function (require, module, exports) {
            app.directive('signupPages', function () {
              return {
                restrict: 'E',
                controller: 'signupPagesController',
                templateUrl: 'views/signup-pages.html'
              };
            });
          },
          {}
        ],
        4: [
          function (require, module, exports) {
            app.directive('doSpin', [
              '$animate',
              function ($animate) {
                return function (element, scope, attrs) {
                  element.on('click', function () {
                    $animate.addClass(element, 'spin');
                  });
                };
              }
            ]);
          },
          {}
        ],
        5: [
          function (require, module, exports) {
            app.directive('uploadView', function () {
              return {
                restrict: 'E',
                controller: 'uploadViewController',
                templateUrl: 'views/upload-view.html'
              };
            });
          },
          {}
        ]
      }, {}, [2]));
    },
    {
      './app-header.dir.js': 8,
      './signup-pages.dir.js': 10,
      './spin.dir.js': 11,
      './upload-view.dir.js': 12
    }
  ],
  10: [
    function (require, module, exports) {
      app.directive('signupPages', function () {
        return {
          restrict: 'E',
          controller: 'signupPagesController',
          templateUrl: 'views/signup-pages.html'
        };
      });
    },
    {}
  ],
  11: [
    function (require, module, exports) {
      app.directive('doSpin', [
        '$animate',
        function ($animate) {
          return function (element, scope, attrs) {
            element.on('click', function () {
              $animate.addClass(element, 'spin');
            });
          };
        }
      ]);
    },
    {}
  ],
  12: [
    function (require, module, exports) {
      app.directive('uploadView', function () {
        return {
          restrict: 'E',
          controller: 'uploadViewController',
          templateUrl: 'views/upload-view.html'
        };
      });
    },
    {}
  ],
  13: [
    function (require, module, exports) {
      (function e(t, n, r) {
        function s(o, u) {
          if (!n[o]) {
            if (!t[o]) {
              var a = typeof require == 'function' && require;
              if (!u && a)
                return a(o, !0);
              if (i)
                return i(o, !0);
              throw new Error('Cannot find module \'' + o + '\'');
            }
            var f = n[o] = { exports: {} };
            t[o][0].call(f.exports, function (e) {
              var n = t[o][1][e];
              return s(n ? n : e);
            }, f, f.exports, e, t, n, r);
          }
          return n[o].exports;
        }
        var i = typeof require == 'function' && require;
        for (var o = 0; o < r.length; o++)
          s(r[o]);
        return s;
      }({
        1: [
          function (require, module, exports) {
            require('./startup.serv.js');
          },
          { './startup.serv.js': 2 }
        ],
        2: [
          function (require, module, exports) {
          },
          {}
        ]
      }, {}, [1]));
    },
    { './startup.serv.js': 14 }
  ],
  14: [
    function (require, module, exports) {
    },
    {}
  ]
}, {}, [1]));